# Lixan Fajardo Code Review

Project [Bid To Move](https://gitlab.com/appetiser/bid-to-move-android)

## The Good

#### Package by feature

This is really good a choice for structuring the project especially when someone browses your code and wanted to know what the app does, he just simply browses the `features` directory.


## The Bad

#### Package by feature uses package by layer

On my comment above, it's good to see that you apply package by feature. It is really easy to navigate the features. However, sub directories in `features` is **grouped into layers**.  This defeats the purpose.

```
  features
  |---- auth
  |---- home
  |---- landing
  |---- main
```

`auth`, `home`, `landing`, `main` are not features instead they are layers.  On the other hand, these are features of the app;

* `account`
* `browse_job`
* `details`
* `myjobs`
* `post_jobs`
* `reports`

Another problem that you will encounter in the future is that what if `account` is not part of `home` screen anymore? Because client just wanted to move it somewhere like it's part of `settings` now.  You package by layer doesn't make sense anymore.


## Suggestions

Better approach to **package by feature** would be to remove the layers and throw all features in `features` directory. Something look like this:

```
  features
  |---- account
  |---- browse_jobs
  |---- details
  |---- forgot_password
  |---- login
  |---- myjob
  |---- post_jobs
  |---- reports
  |---- signup
  
```

With this, it would be very easy to determine what your app does and is very easy to navigate
