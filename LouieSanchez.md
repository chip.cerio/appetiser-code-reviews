# Louie Sanchez Code Review

Project [TravelBuddi](https://gitlab.com/appetiser/travelbuddi-android)

Code review was based on merge request [!138](https://gitlab.com/appetiser/travelbuddi-android/-/merge_requests/138)

The implicit `it` parameter in lambda function is not obvious and not readable when used in multi-line lambda.

On [this lambda](https://gitlab.com/appetiser/travelbuddi-android/-/blob/811ad5a980f9ebcd7f25436365ece4db95219868/app/src/main/java/com/appetiser/travelbuddi/features/auth/authdashboard/AuthDashboardViewModel.kt#L69-72) on file `AuthDashboardViewModel` 

```
flatMap {
  it.isConvert = isChecked
  sessionRepository.saveSession(it)
}
```

and [this lambda](https://gitlab.com/appetiser/travelbuddi-android/-/blob/811ad5a980f9ebcd7f25436365ece4db95219868/app/src/main/java/com/appetiser/travelbuddi/features/main/MainViewModel.kt#L345-350) on file `MainViewModel`

```
.subscribe(
  {
    _mainState.value = when {
      it.token.isEmpty() -> MainActivityState.NotYetLoggedIn(it.isConvert)
      else -> MainActivityState.UserFetched(it.user, it.currency, it.isConvert)
    }
  }
)
```

By looking at `it`, readers will take time to know what type of object is `it`.  

Consider [this option](https://github.com/yole/kotlin-style-guide/issues/10) from [yole](https://github.com/yole), a core Kotlin contributor, that 

> When declaring parameter names in a multiline lambda, put the names on the first line, followed by the arrow and the newline:

So this lamba,

```
flatMap {
  it.isConvert = isChecked
  sessionRepository.saveSession(it)
}
```

will be more readable on this

```
flatMap { session ->
  session.isConvert = isChecked
  sessionRepository.saveSession(session)
}
// I'm assuming that `it` is a Session type
```

and this 

```
.subscribe(
  {
    _mainState.value = when {
      it.token.isEmpty() -> MainActivityState.NotYetLoggedIn(it.isConvert)
      else -> MainActivityState.UserFetched(it.user, it.currency, it.isConvert)
    }
  }
)
```

will become this

```
.subscribe({ session ->
    _mainState.value = when {
      session.token.isEmpty() -> MainActivityState.NotYetLoggedIn(session.isConvert)
      else -> MainActivityState.UserFetched(session.user, session.currency, session.isConvert)
    }
  }
)
```