# Jorge Repaso

Project [Link-U](https://gitlab.com/appetiser/link-u-android)

This code review is based on this merge request [!70](https://gitlab.com/appetiser/link-u-android/merge_requests/70)

## The Good

* good use of Clean Architecture
* good use of separation of concerns



## The Bad

#### Gson Serialized Name

The file [`ClearBadgeCountMessageBody`](https://gitlab.com/appetiser/link-u-android/blob/ce2d2955248c0aa5c70056482ba31d0f23704b40/app/src/main/java/com/appetiser/linku/api/body/ClearBadgeCountMessageBody.kt) uses the annotation `@SerializedName()` but still using the same property name.

```
@SerializedName("badge_count") // can be removed
val badge_count: Int
```

If your team agreed on the same property name used from the json response, then you don't need to use `@SerializedName` annotation. Unless you want to use camel casing instead of snake case, then this annotation is helpful.

```
@SerializedName("badge_count")
val badgeCount: Int  // uses camel case
```

#### Leaking LiveData observers in Fragments

Good that you're using `LiveData` observers in your Fragment however, depending how you use it, `LiveData` may or may not leak it.


Fragments have tricky lifecycle and when a fragment gets detached and re-attached it is not always actually destroyed, for example, retained fragments are not destroyed during configuration changes. When this happens, the fragment’s instance survives and only its view gets destroyed, so onDestroy() is not called and DESTROYED state is not reached.

This means that if we start observing LiveData in onCreateView() or later (typically in onActivityCreated()) and pass Fragment as LifecycleOwner like:

```
viewModel.acceptedJob.observe(this, Observer {
  ...
})
```

we will wind up passing a new identical instance of Observer every time the fragment is re-attached, but LiveData won’t remove previous observers, because LifecycleOwner (Fragment) didn’t reach DESTROYED state. This eventually results in growing number of identical observers being active at the same time and same code from onChanged() being executed multiple times.

The recommended solution is to use fragment’s view lifecycle via getViewLifecycleOwner() or getViewLifecycleOwnerLiveData() which were added in Support Library 28.0.0 and AndroidX 1.0.0, so that LiveData will remove observers every time the fragment’s view is destroyed:

```
viewModel.acceptedJob.observe(viewLifecycleOwner , Observer {
  ...
})
```



### Code Review April 21, 2020

This code review is based on merge request [!190](https://gitlab.com/appetiser/link-u-android/-/merge_requests/190) 

Code looks good and there's a minor improvement that I would suggest.

If multiple `Query`s being passed as parameter, you can also use `Retrofit`'s `QueryMap` annotation. See [here](https://futurestud.io/tutorials/retrofit-2-add-multiple-query-parameter-with-querymap) for more details.


