# TJ Capulong Code Review

Project [construction-zone-android](https://gitlab.com/appetiser/construction-zone-android)

Code review was based on merge request [!133](https://gitlab.com/appetiser/construction-zone-android/-/merge_requests/133)

The implicit `it` parameter in lambda function is not obvious and not readable when used in multi-line lambda.

On [this lambda](https://gitlab.com/appetiser/construction-zone-android/-/blob/2a3aae5c35d094a298f8c7d4c4ca9db49be89a3a/app/src/main/java/com/appetiser/constructionzone/features/main/provider/find_jobs/FindJobsFragment.kt#L144-153) on file `FindJobsFragment` 

```
viewmodel.state.observe(viewLifecycleOwner, Observer { 
  when (it) {
    is FindJobsState.OnRequestUserSuccess -> {
      val photo = 
        if (it.user.profilePhotoUrl == getString(R.string.placeholder_blank_face)) {
          BASE_URL + getString(R.string.placeholder_blank_face)
        } else {
          it.user.profilePhotoUrl
        }
    }
  }
})
```

By looking at `it`, readers will take time to know what type of object is `it`.  

Consider [this option](https://github.com/yole/kotlin-style-guide/issues/10) from [yole](https://github.com/yole), a core Kotlin contributor, that 

> When declaring parameter names in a multiline lambda, put the names on the first line, followed by the arrow and the newline:

So this lamba would be a lot more readable with this

```
viewmodel.state.observe(viewLifecycleOwner, Observer { state -> 
  when (state) {
    is FindJobsState.OnRequestUserSuccess -> {
      val photo = 
        if (state.user.profilePhotoUrl == getString(R.string.placeholder_blank_face)) {
          BASE_URL + getString(R.string.placeholder_blank_face)
        } else {
          state.user.profilePhotoUrl
        }
    }
  }
})
```
