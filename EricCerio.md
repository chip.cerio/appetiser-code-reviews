# Eric Cerio Code Review

Project [Bid To Move](https://gitlab.com/appetiser/bid-to-move-android)

## The Good

#### Package by feature

This is really good a choice for structuring the project especially when someone browses your code and wanted to know what the app does, he just simply browses the `features` directory.

#### Code is well formatted

Ktlint 👍👌💯


## The Bad

#### Functions have no side effects

In the file [BrowseJobViewModel](https://gitlab.com/appetiser/bid-to-move-android/blob/dev/app/src/main/java/com/appetiser/bidtomove/features/home/browse_job/BrowseJobViewModel.kt), there are setters functions.

```
setRange(String)
setKeywords(String)
setCoordinates(Double, Double)
refreshJobsList(Int)
```

While these are descriptive names (which is good), it does hidden things like `setFilters(Int)`

```
fun setRange(range: String) {
    this.range = range
    setFilters(0)    // this is a side effect
}

fun refreshJobsList(page: Int) {
    if (filterKeywordList.isEmpty() && range == "0") {
        getJobs(page)
    } else {
        setFilters(page)    // this is a side effect
    }
}
```

Side effects are lies. Your function promises to do one thing, but it also does other hidden things. They are devious and damaging mistruths that often result in strange temporal couplings and order dependencies.

#### Functions do one thing

The function `getJobs(Int)`, is doing lots more than one thing. It is retrieving access token, jobs, user, and doing transformations.  It should only do one thing which is getting jobs.  After all, the reason we write functions is to decompose a larger concept into a set of steps at the next level of abstraction. 

#### One level of abstraction per function

To make sure functions are doing one thing, we need to make sure that the statements in our function have the same level of abstraction. 

These are the concepts of high level abstraction:

```
fun getJobs(Int) {...}
```

Intermediate level of abstraction:

```
BrowseJobItemDataModel(apiJob).apply {
    id = apiJob.id
    imageUrl = user.profile_photo_url
}
```

Low level of abstraction:

```
_state.value = BrowseJobState.HideProgressLoading
```

Mixing levels of abstraction within a function is always confusing. Readers may not be able to tell  whether a particular expression is an essential concept or a details.

